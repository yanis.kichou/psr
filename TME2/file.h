#ifndef _FILE_H
#define _FILE_H

#include <stdlib.h>
#include<stdio.h>

typedef struct _paquet {
    char * nom;
}paquet;

// creation d'une celulle de la file 
typedef struct _celulle {
    // contenue de la celulle 
    paquet * nom ;
    // pointeurs vers le prochain 
    struct _celulle *suivant ;
}celulle;


// structure de file 
typedef struct _file{
    //celulle de la file 
    celulle * nom;
    //  pointeur sur la tete de la file 
    celulle   * tete;
    // pointeur sur la queue de la fille 
    celulle   * queue;
    // nombre d'elements de al file 
    int nbelement;
}file;

//creation d'une celulle 
celulle * creation_celulle(paquet  *nom){
    if (nom  == NULL ) return NULL;
    // allocation de la memoire pour une cellule 
    celulle * c = (celulle * )malloc(sizeof(celulle));
    //a=remplissage des champs de la celulle 
    c->nom=nom;
    c->suivant=NULL;
    // retourner la celulle cree 
    return c;
}

// creation d'une file 
file * creation_file_vide(){
    // allocation de la memoire pourla file  
    file * p =(file *) malloc(sizeof(file));
    // remplissage des champs de la file 
    p->nom=NULL;
    p->tete=NULL;
    p->queue=NULL;
    p->nbelement=0;
    return p;
}

paquet * creation_paquet(char * nom){
    
    paquet * p = (paquet *) malloc(sizeof(paquet));
    
    if( nom == NULL){
        p->nom ="";
        return p;
    }

    p->nom=nom;
    return p;
}

void enfiler( file *p,paquet *nom  ){
    if( p == NULL || nom == NULL){
        return;
    }
    // allocation de la memoire 
    celulle * nvx =creation_celulle(nom);
    // si le nombre d'element est zero 
    if(p->nbelement==0){
        p->tete=nvx;
        p->queue=nvx;
    }
    // ajout de la nouvelle celulle en queue de file
    p->queue->suivant=nvx;
    p->queue=nvx;
    p->nbelement++;
}
celulle * defiler(file * p){
    // si file vide rien faire ou aucun element 
    if(p == NULL )return NULL;
    if(p->nbelement == 0 ) return NULL;

    celulle *c = p->tete;
    p->tete=c->suivant;
    
    if(p->nbelement == 1){
        p->queue=p->tete;
    }
    p->nbelement--;
    
    return c;
}
int est_file_vide(file *p){
    return p->nbelement == 0 || p == NULL;
}
#endif 