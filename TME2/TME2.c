#include<stdio.h>
#include "file.h"
#include<unistd.h>
#include<sys/types.h>
#include<stdio.h>
#include "fthread.h"
#include<stdlib.h>
#include<pthread.h>
#include "tme2.h"

ft_scheduler_t prod,conso;
ft_thread_t p[Nombre_producteur], c[Nombre_consomation];

void traceinstants ();

int main() {
    //tapis avec capacite 5 pour la file 
    tapis * tapis_prod =creation_tapis(8);
    tapis * tapis_conso = creation_tapis(4);
    

    // compteur de consomation 
    compteur*cpt =creation_compteur (8);
    int i =0;
    
    // reations d'un ordonnanceur 
	  prod = ft_scheduler_create();
    conso = ft_scheduler_create();

    // creations d'un event attacher a cette ordonnanceur 
   	full[0]=ft_event_create(prod); 
    full[1]=ft_event_create(prod); 
    full[2]=ft_event_create(prod); 
    full[3]=ft_event_create(prod); 
    
    messagerprod = ft_event_create(prod);


    // creatiosnd ec evenements du messager et des consomateur 
    empty[0] =ft_event_create(conso);
    empty[1] =ft_event_create(conso);
    empty[2] =ft_event_create(conso);
    empty[3] =ft_event_create(conso);
    
    messagerconso = ft_event_create(conso);

    
    // attachement de la fonction de detection de trace 
    ft_thread_create(prod, traceinstants, NULL, NULL);
    ft_thread_create(conso,traceinstants_consomation,NULL,(void *)cpt);
    
    // creation du messager et l'attacher au scheduler de production dans un premier temps 
    ft_thread_join(ft_thread_create(prod,Messager_run,NULL,(void *)creation_messager(prod,conso,tapis_prod,tapis_conso,cpt)));
  
    // creations des threads producteurs et attacher à l'ordonnanceurs 
    p[0] = ft_thread_create(prod, producteur_run, NULL, (void *)(creation_Producteur(2,0,tapis_prod,"P0",creation_paquet("pomme "))));
    p[1] = ft_thread_create(prod, producteur_run, NULL, (void *)(creation_Producteur(2,1,tapis_prod,"P1",creation_paquet("banane "))));
    p[2] = ft_thread_create(prod, producteur_run, NULL, (void *)(creation_Producteur(2,2,tapis_prod,"P2",creation_paquet("orange "))));
    p[3] = ft_thread_create(prod, producteur_run, NULL, (void *)(creation_Producteur(2,3,tapis_prod,"P3",creation_paquet("poire "))));
    
    // creation des consomateur 
    c[0] = ft_thread_create(conso, consomateur_run, NULL, (void *)(creation_Consomateur(tapis_conso,cpt,"C0",0)));
    c[1] = ft_thread_create(conso, consomateur_run, NULL, (void *)(creation_Consomateur(tapis_conso,cpt,"C1",1)));
    c[2] = ft_thread_create(conso, consomateur_run, NULL, (void *)(creation_Consomateur(tapis_conso,cpt,"C2",2)));
    c[3] = ft_thread_create(conso, consomateur_run, NULL, (void *)(creation_Consomateur(tapis_conso,cpt,"C3",3)));
    
    // join pour attendre la fin des producteur   
    	 ft_thread_join(p[0]);
       ft_thread_join(p[1]);
       ft_thread_join(p[2]);
       ft_thread_join(p[3]);
       
    
    // join pour attendre la fin des consomateur 
    	ft_thread_join(c[0]);
      ft_thread_join(c[1]);
      ft_thread_join(c[2]);
      ft_thread_join(c[3]);
    
    ft_scheduler_start(prod);
    ft_scheduler_start(conso);
    while (cpt->cpt > 0){
      printf("debut \n");
      sleep(10);
    }   
  
  ft_scheduler_stop(prod);
  ft_scheduler_stop(conso);
return 0;
}

void traceinstants () {
  long compteur = 0;
  for (;;) {
    printf(">>>>>>>>>> Scheduler instant %ld :\n", compteur++);
    
    /*
      on ralentit expres pour avoir le temps de voir le deroulement de
      chaque instant en affichage.
    */
    sleep(1);
    
    ft_thread_cooperate ();
  }
} 

