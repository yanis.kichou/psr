package Servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;

import Service.Utilisateur;

/**
 * Servlet implementation class ModifierMotDePasse
 */
@WebServlet("/ModifierMotDePasse")
public class ModifierMotDePasse extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ModifierMotDePasse() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		String clef = request.getParameter("Clef");
		String ancienMotDePasse = request.getParameter("ancienMotDePasse");
		String nouveauMotDePasse= request.getParameter("nouveauMotDePasse");
		try {
			out.print(Utilisateur.modifierMotDePasseUtilisateur(clef, ancienMotDePasse, nouveauMotDePasse));
		} catch (JSONException | SQLException e) {
			out.print(e.getMessage());
		}
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
