package outils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class OutilsVol {

	/** chaine de caracteres pour generation de nom de vols */
	public static String valeursDeLaClef = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

	/**
	 * fonction qui retourne une cahine de caracteres de taille 5 utiliser comme identifiant du vol 
	 * @return
	 */
	public static String generationVol() {
		StringBuffer clef = new StringBuffer();
		for (int i = 0; i < 5; i++)
			clef.append(valeursDeLaClef.charAt(((int) (Math.random() * valeursDeLaClef.length()))));
		return clef.toString();
	}
	
	
	/**
	 * fonction qui teste l'existance d'un vol dans la base de donnée
	 * @param vol : identifiant du vol a checker 
	 * @return : True si le pseudo existe et False sinon 
	 * @throws SQLException
	 */
	public static boolean testExistanceVol( String vol) throws SQLException {
		/** requete sql qui definie l'action de test d'existance d'un vol*/
		String requete="select * from "+Table_Base_De_Donnee.table_vols+" where vol = '"+vol+"' ;";
	
		/** etablissement d'une nouvelle connexion a la base de donnée MYSQL*/ 
		Connection connexion = Database.getMySQLConnection();
		
		/** Creation d'un objet de type statement qui s'occupera d'executer la requete */ 
		Statement statement = connexion.createStatement();
		
		/** execution de la requete de type Query qui recuperer les lignes ou le vol est egale a  celui donner ne  paramettre */ 
		ResultSet resultat = statement.executeQuery(requete);
		
		/** verification si le vol existe deja ou pas */;
		boolean retour = resultat.next();
		
		/** fermeture du resultat*/ 
		resultat.close();
		
		/** destruction de l'objet statement */ 
		statement.close();
		
		/** decconexion de la base de donnée */
		connexion.close();
		
		/** la fonction retournera 0 si l'execution de la fonction n'aboutie a rien */ 
		return retour ;
	
	}
	
	/**
	 * fonction qui teste s'il reste des places dans le vol passer en parametre 
	 * @param vol : identifiant du vol a verifier la disponibilité des places  
	 * @return : True si le pseudo existe et False sinon 
	 * @throws SQLException
	 */
	public static boolean testDisponibiliteVol( String vol) throws SQLException {
		/** requete sql qui le nombre de reservations pour un vol */
		String requete="select count(*) from "+Table_Base_De_Donnee.table_reservation+" where vol = '"+vol+"';";
		
		/** requete sql qui definie l'action de recuperer le nombre de places mximal du vol */
		String requete2="select nombrePlaces from "+Table_Base_De_Donnee.table_vols+" where vol = '"+vol+"';";
		/** etablissement d'une nouvelle connexion a la base de donnée MYSQL*/ 
		Connection connexion = Database.getMySQLConnection();
		
		Statement statement = connexion.createStatement();
		
		/** Creation d'un objet de type statement qui s'occupera d'executer la requete */ 
		Statement statement2 = connexion.createStatement();
		
		/** execution de la requete de type Query qui recuperer les lignes ou le vol est egale a  celui donner ne  paramettre */ 
		ResultSet resultat = statement.executeQuery(requete);
		
		/** execution de la requete de  type query et recuperation du nombre de place du vol */
		ResultSet resultat2 = statement2.executeQuery(requete2);
		
		boolean retour = false;
		
		
		/** verification si le vol a toujours des places disponibles */;
		if(resultat.next()){
			if (resultat2.next()) {
				int nombreReservation 	= resultat.getInt("count(*)");
				int nombrePlace			= resultat2.getInt("nombrePlaces");
				retour = nombreReservation < nombrePlace;
			}
		}
		
		/** fermeture du resultat*/ 
		resultat.close();
		resultat2.close();
		
		/** destruction de l'objet statement */ 
		statement.close();
		statement2.close();
		/** decconexion de la base de donnée */
		connexion.close();
		
		/** la fonction retournera 0 si l'execution de la fonction n'aboutie a rien */ 
		return retour ;
	
	}
	
	

	/**
	 * fonction qui teste l'existance d'une reservation d'un utilisateur dans la base de donnée
	 * @param vol : identifiant du vol a checker 
	 * @param pseudo : pseudo de l'utilisateur 
	 * @return : True si le pseudo existe et False sinon 
	 * @throws SQLException
	 */
	public static boolean testExistanceReservation(String pseudo ,String vol) throws SQLException {
		/** requete sql qui definie l'action de test d'existance d'un vol*/
		String requete="select * from "+Table_Base_De_Donnee.table_reservation+" where vol = '"+vol+"' and pseudo = '"+pseudo+"';";
	
		/** etablissement d'une nouvelle connexion a la base de donnée MYSQL*/ 
		Connection connexion = Database.getMySQLConnection();
		
		/** Creation d'un objet de type statement qui s'occupera d'executer la requete */ 
		Statement statement = connexion.createStatement();
		
		/** execution de la requete de type Query qui recuperer les lignes ou le vol est egale a  celui donner ne  paramettre */ 
		ResultSet resultat = statement.executeQuery(requete);
		
		/** verification si le vol existe deja ou pas */;
		boolean retour = resultat.next();
		
		/** fermeture du resultat*/ 
		resultat.close();
		
		/** destruction de l'objet statement */ 
		statement.close();
		
		/** decconexion de la base de donnée */
		connexion.close();
		
		/** la fonction retournera 0 si l'execution de la fonction n'aboutie a rien */ 
		return retour ;
	
	}

	

}
