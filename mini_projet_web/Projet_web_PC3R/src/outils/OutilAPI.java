package outils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONException;
import org.json.JSONObject;

public class OutilAPI {

	static StringBuffer response=new StringBuffer();
	public static JSONObject  Methode1() throws IOException, JSONException {
		BufferedReader reader ;
		HttpURLConnection connexion ;
		URL url = new URL("http://api.aviationstack.com/v1/flights?access_key=f5462d24f58640a7b1d96fb1eed2982e&&flight_status=active");
		connexion = (HttpURLConnection) url.openConnection();
		
		connexion.getRequestMethod();
		connexion.setConnectTimeout(100000);
		connexion.setReadTimeout(100000);
		
		int status = connexion.getResponseCode();
		System.out.println(status);
		
		if (status > 299) {
			reader = new BufferedReader(new InputStreamReader(connexion.getErrorStream()));
			while (reader.ready()) {
				response.append(reader.readLine());
			}
			reader.close();
		}else {
			reader = new BufferedReader(new InputStreamReader(connexion.getInputStream()));
			while (reader.ready()) {
				response.append(reader.readLine());
			}
			
			reader.close();
		}
		JSONObject j= new JSONObject(response.toString());
		return j;
	}
	public static  String parserDate(String date) {
		String datesp = date.replace("+", "T");
		String [] spliteDate = datesp.split("T");
		return spliteDate[0];
	}
	public static  String parserHeure(String date) {
		String datesp = date.replace("+", "T");
		String [] spliteDate = datesp.split("T");
		return spliteDate[1];
	}
}
