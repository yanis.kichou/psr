package outils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.LocalDateTime;

/**
 * <p>
 * <h3><strong> Class OutilsSession</strong>:</h3>
 * <h4>description</h4>
 * 	qui offre des services liers au verification des informations des Sessions
 * <h4>Service Offerts </h4>
 * 		<ul>
 * 			<li> verification que la Session est ouverte</li>
 * 			<li> verification si un utilisateur existe dejas dans la base de donnée </li>
 * 			<li> recuperation du pseudo apartir de la clef </li>
 * 		</ul>
 * </p>
 * 
 * @author Kichou Galou et Lounis
 *
 */
public class OutilSession {

	public static String valeursDeLaClef = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	/**
	 * generateur de clef de session prennant les caraceteres alphanumeriques possible et generant une clef de 32 caracetrees valide 
	 * @return une clef de session de type String 
	 */
	public static final String generateurClefSession() {
		StringBuffer clef = new StringBuffer();
		for (int i = 0; i < 32; i++)
			clef.append(valeursDeLaClef.charAt(((int) (Math.random() * valeursDeLaClef.length()))));
		return clef.toString();
	}
	
	public static boolean existanceClef(String clef ) throws SQLException {
		/** requete sql qui definie l'action verification du mot de passe */
		String requete="select * from "+Table_Base_De_Donnee.table_session+" where clef = '"+clef+"';";
		
		/** etablissement d'une nouvelle connexion a la base de donnée MYSQL*/ 
		Connection connexion = Database.getMySQLConnection();
		
		/** Creation d'un objet de type statement qui s'occupera d'executer la requete */ 
		Statement statement = connexion.createStatement();
		
		/** execution de la requete de type Query qui permet dde recuperer les lignes de la table Session ou la clef == clef(paramettre) */ 
		ResultSet resultat = statement.executeQuery(requete);
		
		boolean retour=resultat.next();
		
		/** fermeture du resultat*/ 
		resultat.close();
		
		/** destruction de l'objet statement */ 
		statement.close();
		
		/** decconexion de la base de donnée */
		connexion.close();
	
		return retour;
	}
	
	public static boolean validiteClef(String clef ) throws SQLException {
		/** requete sql qui definie l'action verification du mot de passe */
		String requete="select temps from "+Table_Base_De_Donnee.table_session+" where clef = '"+clef+"';";
		
		/** etablissement d'une nouvelle connexion a la base de donnée MYSQL*/ 
		Connection connexion = Database.getMySQLConnection();
		
		/** Creation d'un objet de type statement qui s'occupera d'executer la requete */ 
		Statement statement = connexion.createStatement();
		
		/** execution de la requete de type Query qui permet de recuperer le temps de recuepration de la clef */ 
		ResultSet resultat = statement.executeQuery(requete);
		
		boolean retour = false;
		
		resultat.next();
		long heurActuel=0,date_connection=0;
		
		/** recuperation de l'heure actuel */
		heurActuel=Timestamp.valueOf(LocalDateTime.now()).getTime();
		
		/** recuperation de l'heure de la connexion ou dernere mise a jour de la clef */
		date_connection=resultat.getTimestamp("temps").getTime();
		
		/** verification de temps de non activité 30 mins */
		retour =heurActuel-date_connection<18000000;
		
		/** fermeture du resultat*/ 
		resultat.close();
		
		/** destruction de l'objet statement */ 
		statement.close();
		
		/** decconexion de la base de donnée */
		connexion.close();
	
		return retour;
	}
	
	
	public static String recuperationPseudo(String clef) throws SQLException {
		String pseudo = null ;
		
		/** requete sql qui definie l'action recuperation du pseudo associer a la clef fournie */
		String requete="select pseudo from "+Table_Base_De_Donnee.table_session+" where clef = '"+clef+"';";
		
		/** etablissement d'une nouvelle connexion a la base de donnée MYSQL*/ 
		Connection connexion = Database.getMySQLConnection();
		
		/** Creation d'un objet de type statement qui s'occupera d'executer la requete */ 
		Statement statement = connexion.createStatement();
		
		/** execution de la requete de type Query qui permet de recuperer le temps de recuepration de la clef */ 
		ResultSet resultat = statement.executeQuery(requete);
		
		if(resultat.next()) {
			pseudo =resultat.getString("pseudo");
		}
		
		/** fermeture du resultat*/ 
		resultat.close();
		
		/** destruction de l'objet statement */ 
		statement.close();
		
		/** decconexion de la base de donnée */
		connexion.close();
		
		return pseudo ;
	}
}
