package Service;

import java.sql.SQLException;

import org.json.JSONException;
import org.json.JSONObject;

import outils.OutilSession;
import outils.OutilsUtilisateur;

/**
 * <p>
 * <h3><strong> Class Session</strong>:</h3>
 * <h4>description</h4>
 * 	qui offre des services lier à la gestions des Sessions des utilisateurs de l'application avec les actions compelte
 * <h4>actions  </h4>
 * 		<ul>
 * 			<li> Demarrer d'un session avec  verification des parametres </li>
 * 			<li> Stopper des sessions en verifiant sa validité </li>
 * 			<li> Mise a jour de la clef de session avec verification de la fin de session</li>
 * 		</ul>
 * </p>
 * @author Kichou Galou Lounis
 *
 */
public class Session {
	
	/**
	 * Service permettant de lancer une session a un utilosateur tout en prenanant en compte l'existance du compte utilisateur 
	 * @param pseudo : pseudo duc ompte de l'utilisateur 
	 * @return : JSONObject decrivant la reussite du service ( avec la clef de session ) ou message d'erreur
	 * @throws SQLException
	 * @throws JSONException
	 */
	public static JSONObject demarrerSession(String pseudo,String MotDePasse) throws SQLException, JSONException {
		
		/** JSON object qui definie la reponse du service */
		JSONObject reponse =new JSONObject();
		
		/** si le pseudo n'est vide */
		if (pseudo == null ) {
			/** ajout de l'erreur au message du JSON argument invalide */
			reponse.append("id", 0);
			reponse.append("erreur", 100);
			reponse.append("Cause", "arguments invalide ");
		}else {
			/** si l'utilisateur n'existe pas dans la base e donnée */
			if (!OutilsUtilisateur.testExistanceUtilisateur(pseudo)) {
				/** renvoie d'une erreure à l'utilisateur */
				reponse.append("id", 0);
				reponse.append("erreur",200);
				reponse.append("Cause", "Pseudo n'existe pas ");
			
			}else {
				if (!OutilsUtilisateur.verificationMotDePasse(pseudo, MotDePasse)) {
					/** renvoie d'une erreure à l'utilisateur mot de passe incorrect */
					reponse.append("id", 0);
					reponse.append("erreur",200);
					reponse.append("Cause", "mot de passe incorrecte");
						
				}else {
				/** generation d'une clef de session unique */
				String clef = OutilSession.generateurClefSession();
				/** ajout de la session a la base de donnée */
				BaseDeDonnee.Session.lancerSession(pseudo, clef);
				/** reponse a l'utilisateur avec message de confirmation le lancement de la session */
				reponse.append("id", 1);
				reponse.append("Message", "utilisateur connecter bienvennue  " +pseudo );
				reponse.append("Clef", clef);
				}
			}
		}
		return reponse;
	}

	/**
	 * Service permettant de fermer une session d'un tulisateur valide avec une clef valide 
	 * @param clef: clef de la sessiona  fermer 
	 * @return : JSONObject avec un message de decconection reussite et une errreur sinon 
	 * @throws JSONException
	 * @throws SQLException
	 */
	public static JSONObject stopperSession(String clef) throws JSONException, SQLException {
		/** JSON object qui definie la reponse du service */
		JSONObject reponse =new JSONObject();
		
		/** si la clef n'est vide */
		if (clef == null ) {
			/** ajout de l'erreur au message du JSON argument invalide */
			reponse.append("id", 0);
			reponse.append("erreur", 100);
			reponse.append("Cause", "arguments invalide ");
		}else {
			/** si la session a etait deja lancer pour la stopper */
			if (!OutilSession.existanceClef(clef)) {
				/** renvoie d'une erreure à l'utilisateur */
				reponse.append("id", 0);
				reponse.append("erreur",200);
				reponse.append("Cause", "Session non existante");
			
			}else {
				/** verification de la validite de la clef ( pas de non activite de plus de 30 min )*/
				if (!OutilSession.validiteClef(clef)) {
					reponse.append("id", 0);
					reponse.append("erreur", 400);
					reponse.append("Cause", " temps de non activité depassant 30 mins reconnecter vous ");
					}else {
						/** ajout de la session a la base de donnée */
						BaseDeDonnee.Session.fermerSession(clef);
						/** reponse a l'utilisateur avec message de confirmation le lancement de la session */
						reponse.append("id", 1);
						reponse.append("Message", "utilusateur Déconnecter ....");
				}
			}
		}
		return reponse;
	}
	
	/**
	 * Service de mise a jour d'un clef de session 
	 * @param clef :clef  de la session a mettre a jour  
	 * @return : JSONObject retournant la nouvelle clef de sessin et une erreur sinon 
	 * @throws JSONException 
	 * @throws SQLException 
	 */
	public static JSONObject refraicherSession(String clef) throws JSONException, SQLException {
		/** JSON object qui definie la reponse du service */
		JSONObject reponse =new JSONObject();
		
		/** si la clef n'est vide */
		if (clef == null ) {
			/** ajout de l'erreur au message du JSON argument invalide */
			reponse.append("id", 0);
			reponse.append("erreur", 100);
			reponse.append("Cause", "arguments invalide ");
		}else {
			/** si la session est lancé*/
			if (!OutilSession.existanceClef(clef)) {
				/** renvoie d'une erreure à l'utilisateur */
				reponse.append("id", 0);
				reponse.append("erreur",200);
				reponse.append("Cause", "Session non existante");
			
			}else {
				
				/** verification de la validite de la clef ( pas de non activite de plus de 30 min )*/
				if (OutilSession.validiteClef(clef)) {
					reponse.append("id", 0);
					reponse.append("erreur", 400);
					reponse.append("Cause", " session deja actifs ");
					
					}else {
						String nouvelleclef = OutilSession.generateurClefSession();
						String pseudo = OutilSession.recuperationPseudo(clef);
						
						/** ajout de la session a la base de donnée */
						BaseDeDonnee.Session.nouvelleClefSession(pseudo, nouvelleclef);
						
						/** reponse a l'utilisateur avec message de confirmation le lancement de la session */
						reponse.append("id", 1);
						reponse.append("Message", "clef mise a jour ");
						reponse.append("Clef",nouvelleclef);
				}
			}
		}
		return reponse;
	
	}

}	
