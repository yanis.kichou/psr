package test_unitaire.base_de_donnee;

import static org.junit.Assert.assertTrue;

import java.sql.SQLException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import BaseDeDonnee.Session;
import outils.OutilSession;

public class Session_test {

	final String clef = OutilSession.generateurClefSession();
	String pseudo = "jcvd";
	
	
	@Test
	public void lancerSession() throws SQLException {
		assertTrue(Session.lancerSession(pseudo, clef));
	}
	
	
	@Before
	public void textSession() throws SQLException {
		assertTrue(OutilSession.existanceClef(clef));
	}
	
	@After
	public void fermetureSession() throws SQLException {
		assertTrue(Session.fermerSession(clef));
	}
}

