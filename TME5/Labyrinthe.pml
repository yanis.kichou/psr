mtype{ SUD, OUEST, NORD, EST, ENTRE, SORTIE}
chan go =[0] of {mtype}

active proctype observteurs() {
    mtype dir ;
    do :: go?(dir) ->
    if 
    :: dir == SORTIE -> printf("go Sortie "); goto exit
    :: else -> printf("go %e\n",dir)
    fi
    od
    exit: false
}

active proctype labyrinthe(){
    go!ENTRE ;
    goto c_5_5;

    c_1_1 : 
     if :: true -> go!SORTIE ; goto exit 
        :: true -> go!SUD ; goto c_1_2
        :: true -> go!EST ; goto c_2_1
    fi

    c_2_1 :
     if :: true -> go!EST ; goto c_3_1 
        :: true -> go!OUEST ; goto c_1_1
     fi

    c_3_1: 
     if :: true -> go!EST ; goto c_4_1 
        :: true -> go!OUEST ; goto c_2_1
     fi
    
    c_4_1: 
     if :: true -> go!SUD ; goto c_4_2 
        :: true -> go!OUEST ; goto c_3_1
     fi
    
    c_5_1:
     if :: true -> go!SUD ; goto c_5_2
     fi

    c_1_2:
     if :: true -> go!NORD ; goto c_1_1 
     fi
    
    c_2_2:
     if :: true -> go!EST ; goto c_3_2 
        :: true -> go!SUD ; goto c_2_3
     fi
    
    c_3_2:
     if :: true -> go!EST ; goto c_4_2 
        :: true -> go!OUEST ; goto c_2_2
     fi
    
    c_4_2:
     if :: true -> go!NORD ; goto c_4_1 
        :: true -> go!OUEST ; goto c_3_2
     fi
    
    c_5_2:
     if :: true -> go!NORD ; goto c_5_1 
        :: true -> go!SUD ; goto c_5_3
     fi
    
    c_1_3:
     if :: true -> go!EST ; goto c_2_3 
     fi

    c_2_3:
     if :: true -> go!NORD ; goto c_2_2 
        :: true -> go!OUEST ; goto c_1_3
        :: true -> go!SUD ; goto c_2_4
     fi
    
    c_5_3:
     if :: true -> go!NORD ; goto c_5_2 
        :: true -> go!SUD ; goto c_5_4
     fi

    c_2_4:
     if :: true -> go!NORD ; goto c_2_3 
        :: true -> go!OUEST ; goto c_2_4
        :: true -> go!SUD ; goto c_2_5
     fi

    c_3_4:
     if :: true -> go!EST ; goto c_4_4 
        :: true -> go!OUEST ; goto c_2_4
     fi
    c_4_4:
     if :: true -> go!EST ; goto c_5_4 
        :: true -> go!OUEST ; goto c_3_4
     fi

    c_5_4:
     if :: true -> go!NORD ; goto c_5_3 
        :: true -> go!OUEST ; goto c_4_4
        :: true -> go!SUD ; goto c_5_5
     fi

    c_2_5:
     if :: true -> go!NORD ; goto c_2_4 
        :: true -> go!EST ; goto c_3_5
     fi

    c_3_5:
     if :: true -> go!EST ; goto c_2_5 
        :: true -> go!OUEST ; goto c_4_5
     fi
    
    c_4_5:
     if :: true -> go!OUEST ; goto c_3_5
     fi
    
    c_5_5:
     if :: true -> go!NORD ; goto c_5_4 
     fi

    exit:
        false

    
}