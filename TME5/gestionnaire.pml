mtype { PRODUCTEUR, OUVRIER ,SORTIE}
chan file =[0] of {mtype}
active proctype gestionnaire(chan depuis_ouvriers; chan depuis_producteur; chan vers_ouvrier ){

    mtype paquet 
    do
        :: depuis_producteur?(paquet) -> vers_ouvrier!(paquet)
        :: depuis_ouvriers?(paquet) -> vers_ouvrier!(OUVRIER)
    od
}

proctype producteur(chan vers_gestionnaire ){
    int i;
    vers_gestionnaire!PRODUCTEUR
    
}

proctype collecteur(chan depuis_ouvriers){
    chan journal = [0] of {mtype}
    mtype paquet
    do 
    :: depuis_ouvriers?(paquet) ;
    if 
        :: paquet != SORTIE ->  printf("go %e\n",journal)
        :: else -> goto exit
    fi
    od
    exit:
     
}

proctype ouvrier(chan depuis_gestionnaire; chan vers_gestionnaire; chan vers_collecteur ){
    mtype paquet  
    do :: depuis_gestionnaire?paquet ->
        if  
            :: paquet == PRODUCTEUR ->vers_gestionnaire!paquet;
            :: paquet == OUVRIER -> vers_collecteur!paquet 
            
        fi
    od
}
init{
    chan ouvriers = [10] of {mtype}
    chan collect = [0] of {mtype}
    chan vers_ouvrier = [10] of {mtype}
    chan producteurs = [10] of {mtype}
    
    run collecteur(collect)
    run gestionnaire(ouvriers,producteurs,vers_ouvrier)
    int i;
    int j;
    for (i: 1.. 10){
        for (j: 1..3){
            run producteur(producteurs)
        }
            run ouvrier(vers_ouvrier,ouvriers,collect)
        for (j: 1..3){
            run producteur(producteurs)
        }
    }
} 