/*
 * Binomes :
 * 	- KICHOU yanis 3703169	  
 *  - GALOU Arezki 3702608
 * 
 * 
 */
package TME1.Classes;

public class Paquet {

	// variable de class stockant le contenue du paquet 
	private String s ;
	
	// constructeur qui permet d'initaliser le contenue 
	public Paquet(String s ) {
		this.s=s;
	}
	
	// accesseur au contenue du paquet 
	public String getContenue() {
		return s;
	}
}
