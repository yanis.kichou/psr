    
// import des bibliotheque necessaire pour les threads et mutex et conditions  

use std::collections::VecDeque;
use std::sync::{Arc, Mutex,Condvar};
use std::thread;
// stucture paquet qui permet de modeliser un paquet contenant un String comme nom 

struct Paquet{
   pub nom :String
}
// fonction appliquer au paquet ( creation paquet )
impl Paquet {
    fn new(s:String )-> Paquet {
        Paquet {
            nom : s
        }
    }
}

// structure de tapis qui prends comme argument une file (FIFO) et une capacité ainsi qu'une taille representant la taille maximale de la file 
 struct Tapis {
    file : VecDeque<Paquet>,
    capacite:i64,
    taille: i64
}
// operation sur les tapis 
impl Tapis {
    // fonction de creations d'une instance de Tapis 
    fn new (c : i64 )-> Tapis {
        Tapis {
            file :  VecDeque::new(),
            capacite:c,
            taille: 0
        }       
    }
    // fonction qui renvoie vrai le tapis est vide (File  vide )
    fn is_empty(& self) -> bool {
        self.file.is_empty()
    }

    // fonction qui renvoie vrai si le tapis est remplie (Full)
    fn is_full(& self) -> bool {
        self.taille == self.capacite
    }
}
// deux constantes definisant le nombre de consomateurs et producteurs 
const NOMBRE_CONSOMMATEURS : i64 = 8;
const NOMBRE_PRODUCTEURS: i64 = 2;
const NOMBRE_PRODUCTION : i64 = 4;
const CAPACITE_TAPIS: i64 = 4;
// fonction main 
fn main(){

    // nombre total de consomation des consomateur ( conditions de  boucle ) 
    // tableau de consomations 
    let produits = ["pomme","banane ","poire","orange"];

    // creation du tapis 
    let  t  = Tapis::new(CAPACITE_TAPIS);
    // creation du verou 
    let arc = Arc::new (
        (
            // creation un mutex sur le tapis 
            Mutex::new(t),
            // creation d'un condition (file d'attente en cas de blockage )
            Condvar::new()
        )
    );  
    let mut n : usize = 0;
    //vecteur contenant tout les threads 
    let mut les_threads=vec![];
    // lancement des threads consomateurs 
    for  i in 0..NOMBRE_PRODUCTEURS{
        let tapis_mut = Arc::clone(&arc);
        les_threads.push(
            thread::spawn( move ||{
                for _j in 0..NOMBRE_PRODUCTION{
                    let paquet = Paquet::new(String::from(produits[n]));
                    n=(n+1)%4;
                     // recuperation du verou (lock) et de la condition de blocage (cvar)
                    let (lock, cvar) = &*tapis_mut;
                    // recuperation du tapis contenu dans le Arc 
                    let mut tapis  =lock.lock().unwrap();
                    // verification sur la condition de blocage 
                    // si le tapis st plein pas de possibilité d'enfilement 
                    while  tapis.is_full() {
                        // lacher le verou (lock ) et attendre le prochain defilement afin d'enfiler a nouveau 
                        tapis = cvar.wait(tapis).unwrap();
                    }
                    // incrementation du nombre de paquet dans le tapis 
                    tapis.taille=tapis.taille+1;
                    // ajout du paquet en tete de la file du tapis 
                    println!("P{} {} ",i,paquet.nom);
                    tapis.file.push_back(paquet);
                    // notifier tout les threads bloquer 
                    cvar.notify_all();
                }
            })
        );
    }

    // lacement des threads consomateur 
    for  i in 0..NOMBRE_CONSOMMATEURS{
        let tapis_mut = Arc::clone(&arc);
        les_threads.push(
            thread::spawn(move ||{
                    // recuperation du verou  et de la condition permettant le blockage 
                    let (lock, cvar) = &*tapis_mut;
                    // recuperation du tapis apartir du Arc 
                    let mut tapis  =lock.lock().unwrap();
                    // condition de blockage tapis vide impossibilité de defiler 
                    while  tapis.is_empty() {
                        // lacher le verou temporairement et attendre le prochain enfilement 
                        tapis = cvar.wait(tapis).unwrap();
                    }
                    
                    // decrementation du nombre d'elment dans le tapis 
                     tapis.taille-=1;
                    // recuperation du paquet en tete de file       
                    let p =tapis.file.pop_front().unwrap();
                    // reveiller tout lres thread dormant 
                    cvar.notify_all();
                    println!("C {} mange {}",i,p.nom);
                

            })
        );
    }
    for t in les_threads  {
        t.join().unwrap();
    }
}