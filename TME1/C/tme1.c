#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include "tme1.h"

pthread_t p,c,p2,c2;

int main() {
    //tapis avec capacite 5 pour la file 
    tapis * t =creation_tapis(8,8);

    
    // arguments  pour les thread producteurs  
    Producteur *arg =creation_Producteur(4, t,"pomme");
    Producteur *arg1 =creation_Producteur(4, t,"banane");
    
    //arguments pour les thread consomateurs
    Consomateur *argc=creation_Consomateur(t,"C1");
    Consomateur *argc1=creation_Consomateur(t,"C2");
  
    // lacement des thread producteurs & consomateurs 
    pthread_create(&p, NULL,(void *)producteur,(void *)arg);
    pthread_create(&p2, NULL,(void *)producteur,(void *)arg1);
    pthread_create(&c, NULL,(void *)consomateur,(void *)argc);
    pthread_create(&c2, NULL,(void *)consomateur,(void *)argc1);

    // appel a la fonction join qui permet d'attendre la fin de l'executions des threads 
    pthread_join(p, NULL);
    pthread_join(p2, NULL);
    pthread_join(c, NULL);
    pthread_join(c2, NULL);
    
    return 0;
}