package main

import (
	"bufio"
	"fmt"
	"math/rand"
	"os"
	"regexp"
	"strconv"
	"time"

	st "./structures" // contient la structure Personne
	// contient les fonctions de travail sur les Personnes
	tr "./travaux"
)

var adresse string = "localhost"                          // adresse de base pour la Partie 2
var fichierSource string = "./conseillers-municipaux.txt" // fichier dans lequel piocher des personnes
var tailleSource int = 450000                             // inferieure au nombre de lignes du fichier, pour prendre une ligne au hasard
var tailleG int = 5                                       // taille du tampon des gestionnaires
var nBG int = 2                                           // nombre de gestionnaires
var nBP int = 2                                           // nombre de producteurs
var nBO int = 4                                           // nombre d'ouvriers
var nBPD int = 2                                          // nombre de producteurs distants pour la Partie 2

var pers_vide = st.Personne{Nom: "", Prenom: "", Age: 0, Sexe: "M"} // une personne vide

// paquet de personne, sur lequel on peut travailler, implemente l'interface personne_int
type personne_emp struct {
	person                 st.Personne
	ligne                  int
	afaire                 [](func(st.Personne) st.Personne)
	status                 string
	channelDeLecture       chan int
	channelDeLectureRetour chan string
}

// paquet de personne distante, pour la Partie 2, implemente l'interface personne_int
type personne_dist struct {
	// A FAIRE
}

// interface des personnes manipulees par les ouvriers, les
type personne_int interface {
	initialise()          // appelle sur une personne vide de statut V, remplit les champs de la personne et passe son statut à R
	travaille()           // appelle sur une personne de statut R, travaille une fois sur la personne et passe son statut à C s'il n'y a plus de travail a faire
	vers_string() string  // convertit la personne en string
	donne_statut() string // renvoie V, R ou C
}

// fabrique une personne à partir d'une ligne du fichier des conseillers municipaux
// à changer si un autre fichier est utilisé
func personne_de_ligne(l string) st.Personne {
	separateur := regexp.MustCompile("\u0009") // oui, les donnees sont separees par des tabulations ... merci la Republique Francaise
	separation := separateur.Split(l, -1)
	naiss, _ := time.Parse("2/1/2006", separation[7])
	a1, _, _ := time.Now().Date()
	a2, _, _ := naiss.Date()
	agec := a1 - a2
	return st.Personne{Nom: separation[4], Prenom: separation[5], Sexe: separation[6], Age: agec}
}

// *** METHODES DE L'INTERFACE personne_int POUR LES PAQUETS DE PERSONNES ***

func (p *personne_emp) initialise() {
	p.channelDeLecture <- p.ligne
	ligne := <-p.channelDeLectureRetour
	p.person = personne_de_ligne(ligne)
	nombreTravail := rand.Intn(5) + 1
	i := 0
	p.afaire = make([]func(st.Personne) st.Personne, nombreTravail)
	for i < nombreTravail {
		p.afaire[i] = tr.UnTravail()
		i = i + 1
	}
	p.status = "R"
}

func (p *personne_emp) travaille() {
	if len(p.afaire) == 0 {
		tache := p.afaire[0]
		p.person = tache(p.person)
		p.afaire = p.afaire[:len(p.afaire)-1]
		if len(p.afaire) == 0 {
			p.status = "C"
		}
	}
}

func (p *personne_emp) vers_string() string {
	return "nom " + p.person.Nom + " prenom " + p.person.Prenom + " age " + strconv.Itoa(p.person.Age) + " sexe " + p.person.Sexe + "\n"
}

func (p *personne_emp) donne_statut() string {
	return p.status
}

// *** METHODES DE L'INTERFACE personne_int POUR LES PAQUETS DE PERSONNES DISTANTES (PARTIE 2) ***
// ces méthodes doivent appeler le proxy (aucun calcul direct)

func (p personne_dist) initialise() {
	// A FAIRE
}

func (p personne_dist) travaille() {
	// A FAIRE
}

func (p personne_dist) vers_string() string {
	return " "
}

func (p personne_dist) donne_statut() string {
	return " "
}

// *** CODE DES GOROUTINES DU SYSTEME ***

// Partie 2: contacté par les méthodes de personne_dist, le proxy appelle la méthode à travers le réseau et récupère le résultat
// il doit utiliser une connection TCP sur le port donné en ligne de commande
func proxy() {
	// A FAIRE
}

// Partie 1 : contacté par la méthode initialise() de personne_emp, récupère une ligne donnée dans le fichier source
func lecteur(line chan int, versTravailleur chan string, stop chan int) {
	fichier, _ := os.Open(fichierSource)
	r := bufio.NewReader(fichier)
	ligne, _ := r.ReadString('\n')
	for true {
		select {
		case lastNum := <-line:
			lastLine := 1
			for lastLine < lastNum {
				ligne, _ = r.ReadString('\n')
				lastLine++
			}
			versTravailleur <- ligne

		case <-stop:
			break

		}
	}
}

// Partie 1: récupèrent des personne_int depuis les gestionnaires, font une opération dépendant de donne_statut()
// Si le statut est V, ils initialise le paquet de personne puis le repasse aux gestionnaires
// Si le statut est R, ils travaille une fois sur le paquet puis le repasse aux gestionnaires
// Si le statut est C, ils passent le paquet au collecteur
func ouvrier(depuisGestionnaire chan personne_int, versGestonnaire chan personne_int, versCollecteur chan personne_int, finTime chan int) {
	for true {
		select {
		case paquet := <-depuisGestionnaire:
			if paquet.donne_statut() == "C" {
				versCollecteur <- paquet
			} else if paquet.donne_statut() == "V" {
				paquet.initialise()
				versGestonnaire <- paquet
			} else {
				paquet.travaille()
				versGestonnaire <- paquet
			}
		case <-finTime:
			break
		}
	}
}

// Partie 1: les producteurs cree des personne_int implementees par des personne_emp initialement vides,
// de statut V mais contenant un numéro de ligne (pour etre initialisee depuis le fichier texte)
// la personne est passée aux gestionnaires
func producteur(channel_de_lecture chan string, to_gestionnaire chan personne_emp) {
	var personne personne_emp
	personne.ligne = rand.Intn(tailleSource)
	personne.status = "V"
	to_gestionnaire <- personne
}

// Partie 2: les producteurs distants cree des personne_int implementees par des personne_dist qui contiennent un identifiant unique
// utilisé pour retrouver l'object sur le serveur
// la creation sur le client d'une personne_dist doit declencher la creation sur le serveur d'une "vraie" personne, initialement vide, de statut V
func producteur_distant() {
	// A FAIRE
}

// Partie 1: les gestionnaires recoivent des personne_int des producteurs et des ouvriers et maintiennent chacun une file de personne_int
// ils les passent aux ouvriers quand ils sont disponibles
// ATTENTION: la famine des ouvriers doit être évitée: si les producteurs inondent les gestionnaires de paquets, les ouvrier ne pourront
// plus rendre les paquets surlesquels ils travaillent pour en prendre des autres
func gestionnaire(depuisproducteurs chan personne_int, versouvriers chan personne_int, depuisOuvrier chan personne_int, fintemps chan int ) {
	
	var fileProducteur [] personne_int
	var fileOuvrier [] personne_int
	// avoir un tableau de fonction 
	
	for true { 
		select {
			case paquet := <-depuisproducteurs:
				fileProducteur = append(fileProducteur, paquet)
			case paquet := <- depuisOuvrier:
				fileOuvrier = append(fileOuvrier, paquet)
			case <- fintemps:
				break
		}
	}
}

// Partie 1: le collecteur recoit des personne_int dont le statut est c, il les collecte dans un journal
// quand il recoit un signal de fin du temps, il imprime son journal.
func collecteur(fin_temps chan int, paquets_recu chan personne_int) {
	journal := ""
	for true {
		select {
		case <-fin_temps:
			fmt.Println("Journal \n" + journal)
		case paquet := <-paquets_recu:
			journal = journal + paquet.vers_string() + "\n"
		}
	}
}

func main() {
	rand.Seed(time.Now().UTC().UnixNano()) // graine pour l'aleatoire
	if len(os.Args) < 3 {
		fmt.Println("Format: client <port> <millisecondes d'attente>")
		return
	}
	//port, _ := strconv.Atoi(os.Args[1])   // utile pour la partie 2
	millis, _ := strconv.Atoi(os.Args[2]) // duree du timeout
	fintemps := make(chan int)
	// A FAIRE
	// creer les canaux
	// lancer les goroutines (parties 1 et 2): 1 lecteur, 1 collecteur, des producteurs, des gestionnaires, des ouvriers
	// lancer les goroutines (partie 2): des producteurs distants, un proxy
	time.Sleep(time.Duration(millis) * time.Millisecond)
	fintemps <- 0
	<-fintemps
}
